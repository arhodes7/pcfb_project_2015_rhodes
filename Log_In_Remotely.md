##Amazon Instance for the Class

In order to practice logging into servers remotely, I have created a free Amazon instance that simulates a remote server for our purposes.

An instance is a remote server on the amazon cloud that allows you to develop a set of tools that you can share with colleagues even if they are not on your cluster.

For example, I can log in to the Oregon State University cluster, but you cannot.

Amazon has these tools to allow people to emulate a cluster experience from your own computer.

However, high throughput computing, such as next gen sequencing analysis, would cost a lot of money because the required instance would need to be big enough to allow programs to run that have high levels of memory.

What is nice, however, is that you can check out an instance temporarily for free.


Two warnings:

*    The instance information is NOT saved indefinitely, you want to take a "snapshot" to backup all of your changes.

*    The instance stays on unless you pause it.  As in many "free" cloud servers, you can be charged if you leave your instance running indefinitely.  So be careful and make sure to pause or stop instances when you are not actively on a cloud.

#Download a file to allow you to login to the instance

Download and install the file PCFB.pem to your computer.

`chmod 400 PCFB.pem` to make sure that your key is only readable.

The PCFB.pem file is in this bitbucket's downloads directory (see cloud with down arrow to the left), but here is a link in case you are having trouble finding it:

[PCFB.pem](https://bitbucket.org/arhodes7/pcfb_project_2015_rhodes/downloads/PCFB.pem)

Once you have the key file for the instance on your computer, you can come and join me in the instance by entering the following command on your temrminal command line.

        ssh -i ~/path/to/PCFB.pem ubuntu@ec2-52-20-119-122.compute-1.amazonaws.com

Once you log in, you will see a similar prompt in your terminal window, but you are on the remote server.

![Cloud Login](https://bitbucket.org/arhodes7/pcfb_project_2015_rhodes/downloads/cloud_login.png)


#For more information, see these resources.  However, not required today.

Amazon has several free options for these tools, so you can definitely play around with the concept if you want to sign up and create instances to share work with collaborators.

If you are very enthusiastic about exploring Amazon instances for yourself, here is a pictorial explanation about how you can set one up.  Please be aware before going into any of the paid services that they will continue to charge you forever if you don't turn off your volumes/instances, etc.

Start an instance (http://angus.readthedocs.org/en/2015/amazon/start-up-an-ec2-instance.html)
