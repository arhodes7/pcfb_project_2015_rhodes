#Advanced Shell 5
##Putting it all together

Once again, make sure you are in the "examples" file.

Make sure that the *.pdb files from the dropbox are in your examples file.

        cd ~/pcfb/examples

What does the following command do?

        head -n 2 *.pdb
        
What do you see in the files?

filename
first line
second line

When you "head" multiple files, it places the name of each file at the top of the lines that come from that file.

We are going to use the file

        structure_1gfl.pdb

This is what the protein looks like:

![1GFL](https://bitbucket.org/arhodes7/pcfb_project_2015_rhodes/downloads/1GFL.png)

        head structure_1gfl.pdb
        
We want to get information related to the amino acids, what word helps us do that?

        grep ATOM structure_1gfl.pdb

We get extraneous lines where ATOM is in the remarks.

Let's deselect those lines.

        grep ATOM structure_1gfl.pdb | grep -v REMARK
        
Notice that when we are piping, we are not repeating the file name each time.

Now we want to extract just the amino acid three letter code and numerical position.

        grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort | uniq
        
To get the most abundant amino acids at the top of the list, then sort in reverse numerically.

        grep ATOM structure_1gfl.pdb | grep -v REMARK | cut -c 18-21,24-26 | sort | uniq -c | sort -nr
        
Now make a bash script and save this function.

Next, we will make an argument for our script that allows us to enter any file into this command.

