#Advanced Shell 4

##A few more simple commands for file manipulation

As we mentioned before, tail and head are only useful for data extraction if you either want to subset a random portion of data for testing scripts, or if you know what you are looking for is at the top or bottom of the file.

One way to be sure that you are getting a certain subset is to use sort

##Sort

The bash command sort can arrange your data alphabetically or numerically.  It uses the first column by default.

        head FPexcerpt.fta
        
Is it in any kind of order? Maybe we only care about the header lines.

        grep ">" FPexcerpt.fta
        
capital letters are first, the default sort order is based on the first character in the first field of data.

        grep ">" FPexcerpt.fta | sort
        
How do I figure out how to ignore capital letters when sorting alphabetically?

        man sort
        
Which flag?

Try it.

What if I don't want to sort by the first column?

        tail -n +2 ThalassocalyceData.txt | sort -k 2
        
Data is sorted by the second "kolumn"

But the sort doesn't make sense, right?

How about specifying a numerical sort?

    tail -n +2 ThalassocalyceData.txt | sort -k 2 -n

If we want to reverse the sort, we can just add -r

##Uniq

Uniq can be useful in determining which pieces of the sorted data are duplicated.

Let's check our FPexamples file to see if any accession numbers are duplicated.

    grep ">" FPexamples.fta

But be careful, uniq needs to be implemented with sort to work correctly.

Let's take a look at the japatella_respirationltxt file for an example.


