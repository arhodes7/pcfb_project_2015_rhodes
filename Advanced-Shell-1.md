#Advanced Shell - 1

##Writing a simpler program

Starting slowly:  Our first shell script:  Hello.sh
---------------------------------------------------------------------------------

Let's write a very simple program.

On our command line, write the following (using your name)

		echo "Hello Adelaide"

Okay, we verified that it worked in INTERACTIVE mode, meaning it runs directly on the command line.

Most things we write on the command line should work in our scripts.

		cd ~
		nano Hello.sh

copy the following two lines into your file, replacing "Adelaide" with your name, of course:
---------------------------------------------------------------
		#!/bin/bash
		echo "Hello Adelaide"
---------------------------------------------------------------
Control-X to exit from nano

Shebang

	#!/bin/tcsh is referred to as a "shebang"

If you have the shebang, then it will be executed using the command interpreter specified in the shebang.  In this case, we are using the tcsh shell program to interpret our commands.

You can have shebangs that look like

	#!/bin/bash
	#!/usr/bin/R

Run "which" to determine how to format your shebang.

		which bash
		which tcsh
		which R

Okay, let's check and see if our bash script works.

		Hello.sh
		
What happened?

Is this an executable file?

Hint (we see more details about the file when we use ls -l)

		ls -l Hello.sh

		-rw-r--r-- 1 rhodesad_ws teaching 34 Apr 28 17:34 Hello.sh

The first space indicates whether this is a directory with the letter "d" if it is and "-" if it is not.

The second three letters indicate user permissions.

I (the user) have permission to read and write this file.

The fifth through seventh spaces indicate the group permissions.  Our group in this class is "teaching"

Your group only has permission to read this file.

The last three spaces indicate the global permissions.

Everyone else can only read this file.

We want to make this file executable at least to ourselves, and probably to our group, since many labs use the same script files.  You can make the file executable to the group, but not writeable to prevent accidental rewrites of your script.  You could do this to the scripts directory as a whole also, but here we are just going to practice this on our individual file.

First, lets make it executable to ourselves.

		chmod u+x Hello.sh

		ls -l to make sure it looks like:

		-rwxr--r-- 1 rhodesad_ws teaching 34 Apr 28 17:34 Hello.sh*


Then, let's make it executable to the group, but not writeable.

Does this program work anywhere else?

Why not?

Move it to the appropriate location in the path

ORRRRRRRRR

Add a symbolic link to this file into your path.

		cd ~/scripts
	
	or to wherever you have been keeping your scripts for the class
	
		ln -s ~/Hello.sh Hello.sh

Let's a symbolic link into our bin so it will run from everywhere in our directory.

Why would I want to do this instead of having an alias or putting the file directly into the scripts directory?
