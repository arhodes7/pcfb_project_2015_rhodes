#Advanced Shell 2

The power of symbolic links in the context of a cluster.

On the last page, we created a symbolic link to our program "Hello.sh"

------------>  The symbolic link will work even if I update the program.  That means I only have to edit the program once.

*    Keeping scripts in a script directory means that we only have to worry about editing one version of the file.

*    Placing a symbolic link when the script is ready makes it possible to use it wherever we are in our filespace.

*    When we change the script file, the symbolic link will find the new version of the program, as long as the name stays the same.

*    We only have to "source" the symbolic link, from then on we can make changes to the program or data the symbolic link points to and it will be updated automatically. Adding a new program to the "local/bin" file directory is different, we still need to "source" it.

Let's test that it works.

        cd ~

        nano Hello.sh

Let's change the program

add the line

        echo "How are you doing?"  

after the Hello statement

Your new program will look like:
-----------------------------------------------------
        #!/bin/tcsh -f
        echo "Hello Adelaide"
        echo "How are you doing?"
-----------------------------------------------------
Control-X to quit nano

        cd ~/pcfb/examples or somewhere else that is not where the program sits
        Hello.sh

You should see:

        Hello Adelaide
        How are you doing?

The program changed, the symbolic link did not.

How could this be useful in a large computational cluster?


