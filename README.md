#Advanced Shell: Scripting

This page will provide notes and tips on advanced shell scripting.

It is often useful to develop short scripts to manage multiple programs in various languages.
Having a generic script to automate repetitive tasks allows you to spend more time for other important tasks.

Bash script has a few different elements that we have been implementing this week.

* **shebang**
     	
```
      #!/bin/bash
```

* **a command**
```
#!bash

cp
pwd
ls
```
*  **some arguments and options**
*  **objects for the command to act upon**
*  **some output**

This is a shell script in simplest terms.
