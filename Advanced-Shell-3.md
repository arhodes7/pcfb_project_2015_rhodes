#Advanced Shell 3

##A few simple commands for file manipulation

        cd ~/pcfb/examples
        
The next few pages use files from the "examples" folder, so please make sure to navigate there.

We may want to look at the top of a file to determine its content and formatting.

We have learned about less previously, but less opens the entire file.

What if we have millions of lines in the file?  That could take a while to page through.

Two commands that create a helpful shortcut are "head" and "tail".

        head ctd.txt

Head allows a view of the top 10 lines of a file.

The default value is 10 lines, but this can be increased:

        head -n 20 ctd.txt
        
The reverse of head is tail.

        tail ctd.txt
        
Tail allows a view of the bottom 10 lines of a file.

I use tail a lot when looking at log files on programs that are running.  The last 10 lines are visible, and I can keep looking into the log file even as it is being created.

Tail can be used as part of a pipeline.

For example, we can look at our recent history.

        history | tail -n 15

Tail can be used to skip over the first line by using a "+" sign in front of the number.

        tail -n +2 ThalassocalyceData.txt

Head and tail are useful if our data is organized in such a way that we can get to the information we need at the top or bottom.

How often is this the case?

Maybe we want to extract lines and columns of interest from a dataset using simple commands.

##Grep

grep is the tool we use to extract lines

        grep ">" FPexamples.fta
        
##Cut

cut is the tool we use to extract columns

        grep ">" FPexamples.fta | cut -c 2-11
        
This command extracted the characters 2 to 11 from our lines, which were not delimited obviously.

What if we wanted to get only the first part of the accession numbers, before the "."?

        grep ">" FPexamples.fta | cut -d . -f 1
        
In this case -d represents the "delimiter" and -f specifies the field we want that is separated by that delimiter.

If we want to get rid of the ">" after this, what command could we type?

        grep ">" FPexamples.fta | cut -d .f 1 | ????????
        
cut recognizes tab as a delimiter so you don't have to specify it.

        cut -f 2-4 ThalassocalyceData.txt
        
If there is a space in the file, you have to give -d " " to have it recognized.

Multiple spaces are treated as multiple separators.

How would you write a command to open the file ctd.txt and get the depth, temperature and salinity fields only for the first 20 lines, using head and cut?
